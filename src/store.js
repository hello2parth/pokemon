import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  state: {
    modal: { bShow: false, sTitle: "", sMsg: "" }
  },
  mutations: {
    setModal(state, oConfig) {
      state.modal = {
        bShow: oConfig.show,
        sTitle: oConfig.title,
        sMsg: oConfig.message
      };
    }
  },
  actions: {},
  strict: debug
});
